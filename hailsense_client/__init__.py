from .hailsense_client import HailsenseClient
from . import hailsense_cli
from . import reference_client


def main():
    hailsense_cli.main()


def reference():
    reference_client.main()


__all__ = ['HailsenseClient']
