#!/usr/bin/env python3

import argparse
import time
from hailsense_client import HailsenseClient


def main():
    parser = argparse.ArgumentParser(description='Hailsense commandline client')
    parser.add_argument('channel', help='Channel to subscribe')
    parser.add_argument('-u', '--username', dest='user', required=True, help='Hailsense client username')
    parser.add_argument('-p', '--password', dest='password', required=True, help='Hailsense client password')
    parser.add_argument('-s', '--status', dest='status_only', action='store_true',
                        help='Show status overview and exit')
    parser.add_argument('--wss', action='store_true', dest='use_wss', help='Connect using the websockets protocol')
    parser.add_argument('--id', dest='id', default='', help='Unique client identifier (required for persistence)')
    parser.add_argument('--id-conversion', dest='conversion_file', default='',
                        help='Convert inNET sensor IDs to your own IDs')

    args = parser.parse_args()

    client = HailsenseClient(args.channel, args.user, args.password, args.id, args.conversion_file, args.use_wss)
    client.on_status = on_status

    if args.status_only:
        client.connect(loop=False)
        time.sleep(3)
        client.disconnect()
    else:
        client.on_measurement = on_measurement
        client.connect(loop=True)


def on_measurement(device_id, measurement):
    print('%s: %s' % (str(device_id), str(measurement)))


def on_status(device_id, status):
    print('STATUS %s: %s' % (str(device_id), str(status)))


if __name__ == '__main__':
    main()
