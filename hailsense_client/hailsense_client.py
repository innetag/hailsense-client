import paho.mqtt.client as mqtt
import logging
import re
import json


class HailsenseClient:
    def __init__(self, channel, user, password, client_id='', id_conversion_file='', wss_connection=False):
        self.__channel = channel
        self.__host = 'hailsense-mqtt.innet.io'
        self.__port = 9001 if wss_connection else 8883
        self.__client = mqtt.Client(client_id=client_id, clean_session=False if len(client_id) > 0 else True,
                                    transport='websockets' if wss_connection else 'tcp')

        if wss_connection:
            self.__client.tls_set()
        else:
            self.__client.tls_set()
        self.__client.username_pw_set(user, password=password)

        self.on_measurement = None
        self.on_status = None

        self.__client.on_connect = self.__on_connect
        self.__client.on_message = self.__on_message

        self.__client_id_extractor = re.compile('^/[A-z]+/([^/]*)', re.M)

        self.__conversion_data = None
        if id_conversion_file:
            try:
                with open(id_conversion_file) as f:
                    self.__conversion_data = json.load(f)
            except FileNotFoundError:
                raise FileNotFoundError('The conversion file could not be found')
            except json.JSONDecodeError:
                raise json.JSONDecodeError('Could not decode conversion file')

    def connect(self, loop=False):
        self.__client.connect(self.__host, port=self.__port)
        if loop:
            self.__client.loop_forever()
        else:
            self.__client.loop_start()

    def disconnect(self):
        self.__client.disconnect()
        self.__client.loop_stop()

    def __on_connect(self, client, userdata, flags, rc):
        logging.debug('Connected with result code ' + str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        self.__client.subscribe('/%s/+/measurements' % self.__channel, 2)  # Subscribe to all sensors
        self.__client.subscribe('/%s/+/status' % self.__channel)  # Subscribe to all status data

    def __on_message(self, client, userdata, msg):
        topic = str(msg.topic)
        message = msg.payload.decode('utf-8')
        logging.debug('Message received.\n' + topic + ': ' + message)

        match = self.__client_id_extractor.search(topic)
        if match:
            device_id = match.group(1)
            if self.__conversion_data:
                try:
                    device_id = self.__conversion_data[device_id]
                except KeyError:
                    logging.error(f'Could not convert {device_id}. Entry not found.')
        else:
            device_id = 'unknown'

        if topic.endswith('measurements'):
            if callable(self.on_measurement):
                self.on_measurement(device_id, message)
        elif topic.endswith('status'):
            if callable(self.on_status):
                self.on_status(device_id, message)
        else:
            logging.warning('Message with topic "%s" could not be routed.' % topic)
