import argparse
from hailsense_client import HailsenseClient
import os


log_root = None


def main():
    parser = argparse.ArgumentParser(description='Hailsense reference client')
    parser.add_argument('channel', help='Channel to subscribe')
    parser.add_argument('-u', '--username', dest='user', required=True, help='Hailsense client username')
    parser.add_argument('-p', '--password', dest='password', required=True, help='Hailsense client password')
    parser.add_argument('--wss', action='store_true', dest='use_wss', help='Connect using the websockets protocol')
    parser.add_argument('--logroot', dest='logroot', default=os.path.join(os.getcwd(), 'logs'),
                        help='The root directory for the logfiles (default: wokrdir/logs)')
    parser.add_argument('--id', dest='id', default='', help='Unique client identifier (required for persistence)')
    parser.add_argument('--id-conversion', dest='conversion_file', default='',
                        help='Convert inNET sensor IDs to your own IDs')

    args = parser.parse_args()

    global log_root
    log_root = args.logroot

    if not os.path.exists(log_root):
        os.makedirs(log_root)

    client = HailsenseClient(args.channel, args.user, args.password, args.id, args.conversion_file, args.use_wss)
    client.on_measurement = on_measurement
    client.on_status = on_status
    client.connect(loop=True)


def on_measurement(device_id, measurement):
    global log_root
    log_path = os.path.join(log_root, device_id + '.log')

    with open(log_path, 'a') as logfile:
        logfile.write(measurement + '\n')


def on_status(device_id, measurement):
    global log_root
    log_path = os.path.join(log_root, 'status.log')

    with open(log_path, 'a') as logfile:
        logfile.write(device_id + ': ' + measurement + '\n')


if __name__ == '__main__':
    main()
