from distutils.core import setup
from os import path
from codecs import open

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name='hailsense_client',
      version='1.2.0',
      description='Write your won hail-sensor receiver applications',
      long_description=long_description,
      long_description_content_type='text/markdown',
      install_requires=[
          'paho-mqtt'
      ],
      license='MIT',
      packages=['hailsense_client'],
      author='Michael Blaettler',
      author_email='michael.blaettler@innetag.ch',
      url='https://bitbucket.org/innetag/hailsense-backend',
      entry_points={
          'console_scripts': [
              'hailsense_cli = hailsense_client:main',
              'hailsense_refclient = hailsense_client:reference'
          ]
      }
      )
